"uses strict";

import { Character } from "./character";
import { Job } from "./job";


let form = document.querySelector('#form-character');

form.addEventListener('submit', function (event) {
  event.preventDefault();

  let inputName = document.querySelector('#name').value;
  let warr = new Character (inputName,'woman', 10 ,new Job('warrior', 2, 1, 3, 'sword'));
  let wiz = new Character (inputName,'man', 7 ,new Job('wizard', 2, 3, 1, 'staff'));
  let arc = new Character (inputName,'woman', 8 ,new Job('archer', 3, 2, 1, 'bow'));

  let inputJob = document.querySelector('.job:checked').value;
  
  if (inputJob === "warrior") {
    let hero = warr;
    console.log(hero);
  } else if (inputJob === "wizard") {
    let hero = wiz;
    console.log(wiz);
  } else if (inputJob === "archer") {
    let hero = arc;
    console.log(arc);
  }

})






