export class Job {
  constructor(job, agility, intelligence, strength, diceValue = [], equipment) {
    this.job = job;
    this.agility = agility;
    this.intelligence = intelligence;
    this.strength = strength;
    this.diceValue = diceValue;

  }
  
  attack() {

    if (this.job === 'warrior') {
      this.throwDice(this.strength);
    }

    else if (this.job === 'wizard') {
      this.throwDice(this.intelligence);
    }

    else {
      this.throwDice(this.agility);
    }
  }
  throwDice(stat) {
    this.diceValue = [];
    for (let index = 0; index < stat; index++) {
      let dice = (1 + Math.floor(Math.random() * 6)) * stat;
      this.diceValue.push(dice);

    }
  }

  defense() {
    this.throwDice(this.agility);
  }
  intActions() {
    this.throwDice(this.intelligence);
  }
  agiActions() {
    this.throwDice(this.agility);
  }
  phyActions() {
    this.throwDice(this.strength);
  }
}