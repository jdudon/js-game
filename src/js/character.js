//classe Mère qui servira créer les propriétés que les personnages auront en commun
export class Character {
  constructor(name, gender, hp = 10, job) {
    this.name = name;
    this.gender = gender;
    this.hp = hp;
    this.job = job;

  }
  attack() {

    if (this.job === 'warrior') {
      this.throwDice(this.strength);
    }

    else if (this.job === 'wizard') {
      this.throwDice(this.intelligence);
    }

    else {
      this.throwDice(this.agility);
    }
  }
  throwDice(stat) {
    this.diceValue = [];
    for (let index = 0; index < stat; index++) {
      let dice = (1 + Math.floor(Math.random() * 6)) * stat;
      this.diceValue.push(dice);

    }
  }

  defense() {
    this.throwDice(this.agility);
  }
  intActions() {
    this.throwDice(this.intelligence);
  }
  agiActions() {
    this.throwDice(this.agility);
  }
  phyActions() {
    this.throwDice(this.strength);
  }

}

