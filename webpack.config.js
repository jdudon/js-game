module.exports = {
  mode: 'development',
  entry: './src/js/main',
  output: {
    filename: 'bundle.js'
  },
  devtool: 'inline-source-map'
}